jQuery(document).ready(function() {
  jQuery('.links li a').on('click', function(e)  {
    var currentTab = jQuery(this).attr('href');
    // Show/Hide Tabs
    jQuery('.tab-content .aside-content').removeClass('tab-active');
    jQuery('#'+currentTab).addClass('tab-active');
    // Change/remove current tab to active
    jQuery(this).parent('li').addClass('active-tab').siblings().removeClass('active-tab');
    e.preventDefault();
  });
  jQuery('li.active').click(function(){
    jQuery('.col-4').animate({width: 'toggle'});
  });
});

function openNav() {
  if(document.getElementById("mainTest").offsetWidth < 721 ) {
    document.body.classList.add("mobile-vport");
    document.getElementById("col-4").style.width = "100%";
  }
}

function closeNav() {
  document.body.classList.remove("mobile-vport");
  document.getElementById("col-4").style.width = "0";
}
